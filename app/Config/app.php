<?php

/**
 * Arquivo de configuração das constantes usadas no sistema
 * @filesource app.php
 * @version 0.0.1
 * @author Bruno Monteiro <bruno.monteirodg@gmail.com>
 * 
 * LAUS DEO
 */

/**
 * Separador de diretórios
 */
define("DS", DIRECTORY_SEPARATOR);
/**
 * Rota pricipal da aplicação. Geralmente é para esta rota que o servidor aponta
 * para acessar a aplicação.
 * @example / Rota padrão de acesso à aplicação. Default value /
 */
define("APP_ROUTE", "/");
/**
 * Nome da aplicação
 */
define("APP_NAME", "Simplex");
/**
 * Versão da aplicação. Default value 0.0.1
 */
define("APP_VERSION", "0.0.1");
/**
 * Diretório padrão de instalação da aplicação (Diretório 'App').
 * Default value ../
 */
define("APP_INSTALL", "../");
/**
 * Diretório padrão de views
 */
define("DIR_VIEWS", APP_INSTALL . 'app/Views/');
/**
 * Diretório padrão de views
 */
define("DIR_CACHE", APP_INSTALL . 'app/Cache/');
