<?php

namespace App\Controllers;

use Simplex\Controllers\ControllerInterface;
use Simplex\Controllers\Controller;

class IndexController extends Controller implements ControllerInterface
{
    public function indexAction()
    {
        $this->views()->render('home');
    }
}
