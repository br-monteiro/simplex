<?php

namespace App\Entities\Common;

trait EntitiesCommon
{
    /**
     * Retorna um objeto JSON
     * @return json
     */
    public function json()
    {
        return json_encode($this->tuple());
    }

    /**
     * Seta todos os valores aos atributos da Entidade
     * @param array $value
     */
    public function setAllAttrib(array $value)
    {
        foreach ($value as $key => $val)
        {
            $this->$key = $val;
        }
    }
}
