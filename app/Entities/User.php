<?php

namespace App\Entities;

/**
 * @Entity @Table(name="users")
 **/
class User
{
    /**
     * This trait provider methods tuple(), json(), setAllAttrib()
     * \App\Entites\Common\EntitiesCommon
     */
    use Common\EntitiesCommon;

    /** @Id @Column(type="integer") @GeneratedValue **/
    private $id;

    /** @Column(type="string", length=100) **/
    private $username;

    /** @Column(type="string", length=100) **/
    private $password;

    public function getId()
    {
       return $this->id;
    }

    public function getUsername()
    {
       return $this->username;
    }

    public function getPassword()
    {
       return $this->password;
    }

    /**
     * Retorna um array ao invés de objetos
     * @return array
     */

    public function tuple()
    {
       return [
            "id" => $this->id,
            "username" => $this->username,
            "password" => $this->password
        ];

    }
}
