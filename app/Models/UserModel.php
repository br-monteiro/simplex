<?php
namespace App\Models;

use Simplex\Models\ORM;
use App\Entities\User;
use Simplex\Models\ModelInterface;

class UserModel extends ORM implements ModelInterface
{

    public function __construct() {
        parent::__construct();
    }

    public function model()
    {
        return $this->getEntityManager()->getRepository(User::class);
    }

    public function findAll()
    {
        return $this->model()->findAll();
    }

    public function find($id)
    {
        return $this->model()->find($id);
    }

    public function create(array $values)
    {
        $entity = new User;
        $entity->setAllAttrib($values);
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }

    public function update($id, array $values)
    {
        $entity = $this->find($id);
        $entity->setAllAttrib($values);
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function delete($id)
    {
        $entity = $this->find($id);
        $this->model()->remove($entity);
        $this->getEntityManager()->flush();
        return true;
    }

    public function findAllToJson()
    {
        $entities = $this->findAll();
        return $this->responseJson($entities);
    }
}
