<?php

namespace App;

class Route
{
    private $router;
    
    /**
     * Use Aura/Router
     * @param \Aura\Router\Router $router
     */
    public function __construct(\Aura\Router\Router $router)
    {
        $this->router = $router;
    }
    
    public function definedRoute()
    {
        // rotas
        $this->router->addGet('home', '/')
            ->addValues([
                'controller' => 'IndexController',
                'action' => 'indexAction'
            ]);
        
        return $this->router;
    }
}
