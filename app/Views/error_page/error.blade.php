<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Erro {{$status or '404'}}</title>
    </head>
    <body>
        <h3>Erro {{$status or '404'}}</h3>
        <div>
            {{$message or 'Puts.. Algo deu errado. Esta página ou arquivo não existe.'}}
        </div>
    </body>
</html>