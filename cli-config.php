<?php
require_once 'vendor/autoload.php';

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Simplex\Models\ORM;

// replace with mechanism to retrieve EntityManager in your app
$configOrm = new ORM();
$configOrm->setEntitiesDir(__DIR__ . '/app/Entities/');
$configOrm->run();

$entityManager = $configOrm->getEntityManager();

return ConsoleRunner::createHelperSet($entityManager);