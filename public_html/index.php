<?php

/**
 * Simplex Framework - Laus Deo
 * @version 0.0.1
 */
require_once '../app/Config/app.php';
require_once '../vendor/autoload.php';

use Simplex\Init;

new Init();